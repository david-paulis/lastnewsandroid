package com.example.devinaboles.newsapp;

/**
 * Created by Devina Boles on 12/07/2017.
 */

public class Article {
    private String stroy;
    private String section;
    private String url;

    public String getStroy() {
        return stroy;
    }

    public String getSection() {
        return section;
    }

    public String getUrl() {
        return url;
    }

    public Article(String stroy, String section, String url) {

        this.stroy = stroy;
        this.section = section;
        this.url = url;
    }
}
