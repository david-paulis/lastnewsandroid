package com.example.devinaboles.newsapp;

import android.content.AsyncTaskLoader;
import android.content.Context;

import org.json.JSONException;

import java.util.List;

/**
 * Created by Devina Boles on 12/07/2017.
 */


public class ArticleLoader extends AsyncTaskLoader<List<Article>> {

    private String mUrl;


    public ArticleLoader(Context context, String url) {
        super(context);
        this.mUrl = url;

    }

    @Override


    public List<Article> loadInBackground() {
        List<Article> articles = null;
        try {
            articles = QueryUtils.fetchBookData(mUrl);
        } catch (JSONException e) {
            e.printStackTrace();

        }
        return articles;
    }


    @Override
    protected void onStartLoading() {
        forceLoad();
    }
}
