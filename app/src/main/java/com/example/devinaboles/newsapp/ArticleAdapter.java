package com.example.devinaboles.newsapp;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

/**
 * Created by Devina Boles on 12/07/2017.
 */

public class ArticleAdapter extends ArrayAdapter<Article> {
    public ArticleAdapter(Context context, List<Article> articles) {

        super(context, 0, articles);
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.list_item, parent, false);
        }

        Article article = getItem(position);
        TextView section = (TextView) convertView.findViewById(R.id.section_name);
        section.setText(article.getSection());
        TextView story = (TextView) convertView.findViewById(R.id.story);
        story.setText(article.getStroy());
        return convertView;


    }
}
