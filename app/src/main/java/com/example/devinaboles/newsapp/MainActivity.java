package com.example.devinaboles.newsapp;

import android.app.LoaderManager;
import android.content.Intent;
import android.content.Loader;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements LoaderManager.LoaderCallbacks<List<Article>> {

    private String mUrl = "http://content.guardianapis.com/search";
    private ArticleAdapter mAdapter;
    private TextView mEmptyView;
    private ListView mList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mAdapter = new ArticleAdapter(this, new ArrayList<Article>());
        mList = (ListView) findViewById(R.id.list);
        mList.setAdapter(mAdapter);
        mEmptyView = (TextView) findViewById(R.id.empty_view);
        mList.setEmptyView(mEmptyView);
        mList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Article article = mAdapter.getItem(position);
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(article.getUrl()));
                if(intent.resolveActivity(getPackageManager())!=null){
                startActivity(intent);}
            }
        });

        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = connectivityManager.getActiveNetworkInfo();
        boolean isConnected = activeNetwork != null && activeNetwork.isConnectedOrConnecting();
        if (isConnected) {
            LoaderManager loaderManager = getLoaderManager();
            loaderManager.initLoader(1, null, this);
        } else {
            View loading_indicator = findViewById(R.id.loading_indicator);
            loading_indicator.setVisibility(View.GONE);
            TextView noNet = (TextView) findViewById(R.id.no_internet);
            noNet.setVisibility(View.VISIBLE);
        }


    }

    @Override
    public Loader<List<Article>> onCreateLoader(int id, Bundle args) {


        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        String section = sharedPreferences.getString("section", "all");
        Uri baseUri = Uri.parse(mUrl);
        Uri.Builder uriBuilder = baseUri.buildUpon();

        if (!section.equals("all")) {
            uriBuilder.appendQueryParameter("section", section);
        }
        uriBuilder.appendQueryParameter("api-key", "test");
        return new ArticleLoader(this, uriBuilder.toString());


    }


    @Override
    public void onLoadFinished(Loader<List<Article>> loader, List<Article> data) {
        mAdapter.clear();
        mEmptyView.setText(R.string.empty_state);
        View loading_indicator = findViewById(R.id.loading_indicator);
        loading_indicator.setVisibility(View.GONE);
        if (data != null) {
            mAdapter.addAll(data);
        }

    }

    @Override
    public void onLoaderReset(Loader<List<Article>> loader) {
        mAdapter.clear();
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            Intent settingsIntent = new Intent(this, SettingsActivity.class);
            startActivity(settingsIntent);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
